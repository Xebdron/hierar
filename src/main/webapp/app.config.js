'use strict';

angular
    .module('lessApp')
    .config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider
                .when('/login', {
                    templateUrl: '/view/login.html',
                    controller: ['$routeParams', '$scope',
                        function LoginController($routeParams, $scope) {

                        }]
                })
                .otherwise('/login');
        }]);